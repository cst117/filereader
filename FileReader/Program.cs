﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileReader
{
    class Program
    {
        static void Main(string[] args)
        {
            ParseFile theFile = new ParseFile("TextExample.txt");
            Console.WriteLine("This text has {0} words", theFile.getTotalWords());
            Console.WriteLine("The first word alphabetically is '{0}'", theFile.getFirstWordAlphabetically());
            Console.WriteLine("The last word alphabetically is '{0}'", theFile.getLastWordAlphabetically());
            Console.WriteLine("All words starting with a vowel:\n\t{0}", theFile.getWordsStartingWithVowel());
            Console.WriteLine("The longest word in the text is '{0}'", theFile.getLongestWord());

            Console.Read();
        }
    }

    class ParseFile
    {
        protected String filename;
        private int totalWords = 0;
        private char[] vowels = {'a', 'e', 'i', 'o', 'u'};
        protected List<string[]> lineList = new List<string[]>();

        public ParseFile(String fName)
        {
            filename = fName;

            processFile();
        }

        private void processFile()
        {
            string[] lineArray;

            // Go through each line of the file
            foreach (String line in System.IO.File.ReadLines(@filename, Encoding.UTF8))
            {
                if (line != "")
                {
                    // Create array of strings from this line
                    lineArray = line.Split(' ');
                    // Add to total word count
                    totalWords += lineArray.Length;
                    // Add this string array to the list
                    lineList.Add(lineArray);
                }
            }
        }

        /**
         * Goes through each line and pulls the first word alphabetically.
         * It stores it in a temp list, re-sorts them and returns the 
         * first alphabetical word.
         */
        public String getFirstWordAlphabetically()
        {
            List<String> tempWordList = new List<string>();

            // Loop through the list, sort it, and then add the first one to 
            // the temp word list so we can use it later.
            foreach (String[] singleLineArray in lineList)
            {
                Array.Sort(singleLineArray);
                tempWordList.Add(singleLineArray[0]);
            }

            tempWordList.Sort();

            return tempWordList[0];
        }

        /**
         * Goes through each line and pulls the last word alphabetically.
         * It stores it in a temp list, re-sorts them and returns the 
         * last alphabetical word.
         */
        public String getLastWordAlphabetically()
        {
            List<String> tempWordList = new List<string>();

            // Loop through the list, sort it, and then add the first one to 
            // the temp word list so we can use it later.
            foreach (String[] singleLineArray in lineList)
            {
                Array.Sort(singleLineArray);
                tempWordList.Add(singleLineArray[singleLineArray.Length - 1]);
            }

            tempWordList.Sort();

            return tempWordList[tempWordList.Count - 1];
        }

        /**
         * Returns the longets word in the text file
         */
        public String getLongestWord()
        {
            String longestWord = "";

            // Loop through the list find the longest word
            foreach (String[] singleLineArray in lineList)
            {
                foreach (String word in singleLineArray)
                {
                    // If this word is longer than the prev saved word, then update it
                    if (word.Length > longestWord.Length)
                        longestWord = word;
                }
            }

            return longestWord;
        }

        /**
         * Counts the number of words that start with vowels
         */
        public String getWordsStartingWithVowel()
        {
            List<String> tempWordList = new List<string>();
            String results = "";

            // Loop through the list
            foreach (String[] singleLineArray in lineList)
            {
                // Loop through the line array
                foreach (String word in singleLineArray)
                {
                    // Check each word and see if the first char is a vowel
                    for (int i = 0; i < vowels.Length; i++)
                    {
                        // If first char is vowel, add it to temp list
                        if (word[0].Equals(vowels[i]))
                        {
                            tempWordList.Add(word);
                        }
                    }
                }
            }

            // Build string to return the words
            for (int k = 0; k < tempWordList.Count; k++)
            {
                if (k != 0)
                    results += ", ";

                results += tempWordList[k];
            }

            return results;
        }

        public int getTotalWords()
        {
            return totalWords;
        }
    }
}
